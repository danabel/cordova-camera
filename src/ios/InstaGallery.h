//
//  InstaGallery.h
//  InstaGallery
//
//  Created by Eugene Stroganov on 6/21/14.
//  Copyright (c) 2014 Test Organization. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IGFirstViewController.h"

@interface InstaGallery : NSObject

+ (UIViewController *)galleryViewControllerWithCompletionBlock:(void(^)(UIImage *image, UIViewController *vc))completionBlock ;

@end
